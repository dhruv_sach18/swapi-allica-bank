# Demo Swapi Project - Allica Bank

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app)
It is created as a test for Allica bank 
@author - Dhruv Sachdeva


## Setup Guide
````
cd directory name
npm install
npm start
````
## Project Structure

1. Components - All common components that are being reused and page specific components
2. CSS - global css files
3. Router - router contains all routing related setup, route arrays and prabably nested route arrays if we scale this application
4. Service - Api services classes
5. Constants - All constants messages as well
6. Pages - All pages




## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.


## Implementation added - 

Character list view

- ~~list all characters from the Star Wars universe (name, gender & home
planet)~~ **Done**
- ~~provide pagination controls to step through the results~~ **Done**
- ~~provide a search field to query by character name~~ **Done**
- ~~clicking a list entry should navigate to the character details page~~ **Done**

Character details view

- ~~display: name, hair colour, eye colour, gender and home planet~~ **Done**
- list the films that the character has appeared in **Not Done due to less time**
- list the starships that the character has piloted **Not Done due to less time**
- provide the ability to add the character to the favourites list if they aren’t
already on it **Done**

Favourites view

- ~~display all characters that have been added to the favourites list (name,
height, gender & home planet)~~ **Done**
- ~~provide the ability to remove characters from the list~~ **Done**
- Bonus: provide the ability to amend the height or gender of a character  **Not Done due to less time**


## Implementation thoughts for pending tasks
1. For listing of characters and character has piloted -  can be done by adding another child components and call the api links and directly putting them as url in those components
2. The ability to amend the height or gender of a character - can be done by opening a modal based on id and or changing values there and we will appending those values on the character but it won't be reflected in our normal list as we are not creating a post request
3. Testing - Basic test cases for improving test coverage can be done

## Improvements

1. Loading states are pending
2. Infinite scroll can be added
3. Some places have any written due to less time - we can added types for them
4. Test cases
5. Scss can be added for css reusability and many other benefits
6. React.memo component for CharacterCard component
7. More code comments
8. Focus on more code quality and performance of the app
9. Pagination component implementation can be much better 
