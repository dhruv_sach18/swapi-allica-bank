import {useReducer} from 'react';
import './App.css';
import './css/bootstrap.css';
import { Context, reducer, initialState } from './store';
import Router from './router/Router';
import routes from './router/routes';
function App() {
	const [state, dispatch] = useReducer(reducer, initialState);

	return (
		<Context.Provider value={{ state, dispatch }}>
				<Router routes={routes} />
		</Context.Provider>
	);
}

export default App;
