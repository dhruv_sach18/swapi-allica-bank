import { useEffect, useState } from 'react';
import CharacterList from '../components/Characters/CharacterList';
import CharacterService from '../service/character-service';
import Pagination from '../components/Pagination';
import SearchInput from '../components/Characters/SearchInput';
import '../components/Characters/style.css';
interface CharactersProps {}

const Characters: React.FunctionComponent<CharactersProps> = () => {
	const [searchQuery, setSearchQuery] = useState('');
	const [currentPage, setCurrentPage] = useState(1);
	const [characters, setCharacters] = useState([]);
	const [paginationData, setPaginationData] = useState({
		count: 0,
		next: null,
		totalpages: 0,
		previous: null
	});

	const onPageChange = (page: number) => {
		setCurrentPage(page);
	};
	const onChangeText = (value: string) => {
		setSearchQuery(value);
	};
	const getCharacterList = async () => {
		try {
			const response = await CharacterService.getCharacters(
				currentPage,
				searchQuery
			);
			setCharacters(response.data.results);
			// I used pagination component from a pre-implemented react project to save time
			// Added pagination implementation according to it.
			// We can use previous and next more extensively
			setPaginationData({
				count: response.data.count,
				totalpages: Math.ceil(Number(response.data.count) / 10),
				next: response.data.next,
				previous: response.data.previous
			});
		} catch {}
	};
	useEffect(() => {
		getCharacterList();
	}, [searchQuery, currentPage]);
	return (
		<div className='container' data-testid='characters-container'>
			<SearchInput onChangeText={onChangeText} />
			<CharacterList characters={characters} />
			<Pagination
				total_pages={paginationData.totalpages}
				setPage={onPageChange}
			/>
		</div>
	);
};

export default Characters;
