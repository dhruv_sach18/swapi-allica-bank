import React from 'react';
import Text from '../components/Text';
import colors from '../constants/colors';
interface HomeProps {}

const Home: React.FunctionComponent<HomeProps> = () => {
	console.log('rendered')
	return (
		<div className='d-flex flex-1 align-items-center justify-content-center h-100vh bg-black'>
			<Text size={36} weight={700} color={colors.text.yellow}>
				Welcome to Star Wars
			</Text>
		</div>
	);
};

export default Home;
