import {useContext} from 'react';
import CharacterList from '../components/Characters/CharacterList';
import { Context } from '../store';
interface FavouritesProps {}

const Favourites: React.FunctionComponent<FavouritesProps> = () => {
	const { state } = useContext(Context);
	return (
		<div className='container'>
			<CharacterList characters={state.favourites || []} />
		</div>
	);
};

export default Favourites;
