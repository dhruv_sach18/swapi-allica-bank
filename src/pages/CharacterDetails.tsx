import { useState, useEffect, useContext } from 'react';
import CharacterService from '../service/character-service';
import Text from '../components/Text';
import { Character } from '../constants/interfaces';
import { Context } from '../store';
import { useParams } from 'react-router';

interface CharacterDetailsProps {}

const CharacterDetails: React.FunctionComponent<CharacterDetailsProps> = () => {
	const { state, dispatch } = useContext(Context);
	// get params to get current character id
	const params =useParams()
	const [characterDetails, setCharacterDetails] = useState({
		name: '',
		gender: '',
		hair_color: '',
		eye_color: ''
	});

	// Get character details using charcter id
	const getCharacterDetails = async () => {

		// current character id 
		const id=params.id;
		try {
			
			const response = await CharacterService.getCharacter(id||'1');
			setCharacterDetails(response.data);
		} catch {}
	};
	// Need to find is favourite here
	const isFavourite = (state.favourites || []).find(
		(favourite: Character) => favourite.name === characterDetails. name
	);
	
	// use to toggle state of character  
	// Remove or add to favourites list
	const toggleFavouriteCharacter = (e: React.SyntheticEvent) => {
		e.preventDefault();
		dispatch({
			type: !isFavourite ? 'addFavourite' : 'removeFavourite',
			favouriteCharacter: characterDetails
		});
	};
	
	useEffect(() => {
		getCharacterDetails();
	}, []);

	return (
		<div className='container'>
			<div className='row no-gutters justify-content-between align-items-center'>
				<div>
					<Text>Name: {characterDetails?.name}</Text>
					<Text>Hair Color: {characterDetails?.hair_color}</Text>
					<Text>Gender: {characterDetails?.gender}</Text>
					<Text>Eye Color: {characterDetails?.eye_color}</Text>
				</div>
				<button
				type='button'
				className='favourite-button'
				onClick={toggleFavouriteCharacter}
			>
				<Text size={10}>
					{!isFavourite ? 'Mark as Favourite' : 'Remove from favourite'}
				</Text>
			</button>
			</div>
			
		</div>
	);
};

export default CharacterDetails;
