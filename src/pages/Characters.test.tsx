import React from 'react';
import { render, screen } from '@testing-library/react';
import Characters from './Characters';

test('Renders Characters component', () => {
	const { getByTestId } = render(<Characters />);
	expect(getByTestId('characters-container')).toMatchSnapshot();
});

test('It has search-input element', () => {
	const { getByTestId } = render(<Characters />);
	const searchInputElement = getByTestId('SearchInput');
	expect(getByTestId('characters-container')).toContainElement(
		searchInputElement
	);
});
