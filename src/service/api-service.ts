import axios from 'axios';

const instance = axios.create({
	baseURL: 'https://swapi.dev/api/'
});

export default class APIService {
	// get data from server
	static get(url: string, responseType?: any) {
		return instance({
			url: url,
			responseType,
			method: 'GET'
		});
	}
	// send data to server
	static post(url: string, postBody: any, responseType?: any) {
		return instance({
			url: url,
			method: 'POST',
			responseType,
			data: postBody
		});
	}
	// send complete new data to update existing data
	static put(url: string, putBody: any) {
		return instance({
			url: url,
			method: 'PUT',
			data: putBody
		});
	}
	// only send data that need to change rather than sending complete data
	static patch(url: string, data: any) {
		return instance({
			url: url,
			method: 'PATCH',
			data
		});
	}
	// delete existing data from server
	static delete(url: string, data: any) {
		return instance({
			url: url,
			method: 'DELETE',
			data
		});
	}
}
