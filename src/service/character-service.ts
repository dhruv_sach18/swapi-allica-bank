// This service is used to manage swapi people api's
// It contains following methods
/**
 *
 * Get charachters @method getCharacters
 * Get charachter Details @method getCharacterDetails
 
 * **/
import APIService from './api-service';

export default class CharacterService extends APIService {
	/**
	 *
	 * Get charachters
	 *
	 */
	static getCharacters(currentPage: number, searchQuery?: string) {
		let url = `/people?page=${currentPage}`;
		if (searchQuery) url += `&search=${searchQuery}`;

		return this.get(`${url}`);
	}

	/**
	 *
	 * Get charachter by id
	 * @param {string} id
	 */
	static getCharacter(id: string) {
		let url = `/people/${id}`;
		return this.get(`${url}`);
	}
}
