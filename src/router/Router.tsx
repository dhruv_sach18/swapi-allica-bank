
import {  BrowserRouter } from 'react-router-dom';
import Routing from './Routing';
import Navbar from '../components/Navbar';

interface RouterProps {
	routes: Array<{
		path: string;
		component: React.FunctionComponent;
		children?: React.ReactChildren;
	}>;
}
export default function Router({ routes }: RouterProps) {
	
	return (
		<BrowserRouter>
			<Navbar />
			<Routing routes={routes} />
		</BrowserRouter>
	);
}
