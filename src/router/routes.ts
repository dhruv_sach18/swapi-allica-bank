import Favourites from '../pages/Favourites';
import Characters from '../pages/Characters';
import CharacterDetails from '../pages/CharacterDetails';
import Home from '../pages/Home';
// Not adding private flag as we are not handling authentication
const routes = [
	{ path: '/', component: Home },
	{ path: '/characters', component: Characters },
	{ path: '/character/:id', component: CharacterDetails },
	{ path: '/favourites', component: Favourites }
];
export default routes;
