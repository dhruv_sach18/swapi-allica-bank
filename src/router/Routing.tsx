import React from 'react';
import { Route, Routes } from 'react-router';
interface RoutingProps {
	routes: Array<{
		path: string;
		component: React.FunctionComponent;
		children?: React.ReactChildren;
	}>;
}
const Routing = ({ routes }: RoutingProps) => {
	return (
		<Routes>
			{routes.map(route => {
				const { path, component: Component, children, ...rest } = route;
				return (
					<Route
						{...rest}
						key={path}
						path={`${path}`}
						element={<Component />}
					/>
				);
			})}
		</Routes>
	);
};

export default Routing;
