import { createContext } from 'react';
import { Character } from './constants/interfaces';

export const initialState = {
	favourites: []
};

export const reducer = (
	state: {
		favourites: Array<Character>;
	},
	action: any
) => {
	const { favouriteCharacter, type } = action;

	switch (type) {
		case 'addFavourite':
			return {
				...state,
				favourites: [...state.favourites, favouriteCharacter]
			};
		case 'removeFavourite':
			return {
				...state,
				favourites: state.favourites.filter(
					favourite => favourite.name !== favouriteCharacter.name
				)
			};
		default:
			return state;
	}
};

export const Context = createContext<any | null>(null);
