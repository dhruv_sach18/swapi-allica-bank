
import CharacterCard from './CharacterCard';
import { Character } from '../../../constants/interfaces';
interface CharacterListProps {
	characters: Array<Character>;
}

const CharacterList: React.FunctionComponent<CharacterListProps> = ({
	characters
}) => {
	return (
		<>
			<div className='row character-list-container'>
				{characters.map(({ name, gender, homeworld, url }) => (
					<CharacterCard
						name={name}
						gender={gender}
						homeworld={homeworld}
						key={name}
						url={url}
					/>
				))}
			</div>
		</>
	);
};

export default CharacterList;
