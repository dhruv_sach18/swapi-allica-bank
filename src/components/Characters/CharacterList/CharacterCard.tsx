import * as React from 'react';
import Text from '../../Text';
import {  Link } from 'react-router-dom';
import { Context } from '../../../store';
import { Character } from '../../../constants/interfaces';
interface CharacterCardProps extends Character {
	
}

// Not adding home-planet api here in each card as it will increase the overall calls
// As we only need a string it should be handled from B.E.
// But certainly we can add it here using homeworld url and call for every card
// Or we can make a Set in character list for list of unqiue homeworld and call using Promise.all

const CharacterCard: React.FunctionComponent<CharacterCardProps> = ({
	name,
	gender,
	homeworld,

	url
}) => {
	const { state, dispatch } = React.useContext(Context);
	// need to extract id as we have to navigate it to a new page
	const id=String(url).split('/')?.[5];

	// Need to find is favourite here
	const isFavourite = (state.favourites || []).find(
		(favourite: Character) => favourite.name === name
	);
	const toggleFavouriteCharacter = (e: React.SyntheticEvent) => {
		e.preventDefault();
		dispatch({
			type: !isFavourite ? 'addFavourite' : 'removeFavourite',
			favouriteCharacter: { name, gender, homeworld }
		});
	};

	return (
		<Link to={`/character/${id}`} className='col-md-3'>
			<div className='character-card-container'>
				<div className='row no-gutters justify-content-between'>
					<div>
						<Text weight={700}>{name}</Text>
						<Text size={12}>Gender: {gender}</Text>
					</div>
					<button
						type='button'
						className='favourite-button'
						onClick={toggleFavouriteCharacter}
					>
						<Text size={10}>
							{!isFavourite ? 'Mark as Favourite' : 'Remove from favourite'}
						</Text>
					</button>
				</div>
			</div>
		</Link>
	);
};

export default CharacterCard;
