import { useState, useCallback } from 'react';
import debounce from 'lodash.debounce';
interface SearchInputProps {
	onChangeText: (...args: any) => any;
}

const SearchInput: React.FunctionComponent<SearchInputProps> = ({
	onChangeText
}) => {
	const [value, setValue] = useState('');

	// I have added only 1000ms debounce we can also add more
	// Added useCallback so it memoizes debounce function so it doesn't get recreated again and again
	const debounceSearch = useCallback(debounce(onChangeText, 1000), []);

	// onChange input element event
	// It will set both value for this component and debounce function
	const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		e.preventDefault();
		setValue(e.target.value);
		debounceSearch(e.target.value);
	};
	return (
		<div className='search-input-container'>
			<input
				data-testid='SearchInput'
				placeholder='Type anything here...'
				onChange={onChange}
				value={value}
				className='search-input'
			/>
		</div>
	);
};

export default SearchInput;
