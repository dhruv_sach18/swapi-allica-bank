import React from 'react';
import { render, screen } from '@testing-library/react';
import Navbar from '.';
import { BrowserRouter } from 'react-router-dom';

test('Renders Navbar component', () => {
	const { getByTestId } = render(
		<BrowserRouter>
			<Navbar />
		</BrowserRouter>
	);
	expect(getByTestId('Navbar')).toMatchSnapshot();
});
jest.mock('react-router-dom', () => ({
	...jest.requireActual('react-router-dom'),
	useLocation: () => ({
		pathname: '/'
	})
}));
test('Renders Navbar active class', () => {
	const { getByTestId } = render(
		<BrowserRouter>
			<Navbar />
		</BrowserRouter>
	);
	expect(getByTestId('NavbarItem_h0')).toHaveClass('active');
});

test('Have Navbar Item Characters', () => {
	const { getByText } = render(
		<BrowserRouter>
			<Navbar />
		</BrowserRouter>
	);
	expect(getByText('Characters')).toHaveAttribute('href', '/characters');
});
