import { Link, useLocation } from 'react-router-dom';
import links from './links';
import './style.css';
interface NavbarProps {}

const Navbar: React.FunctionComponent<NavbarProps> = () => {
	const location = useLocation();
	const isActive = (path: string) =>
		location.pathname === path ? 'active' : '';
	return (
		<nav className='bg-black' data-testid='Navbar'>
			<ul className='container'>
				{links.map(({ keyIndex, path, title }) => (
					<li
						key={keyIndex}
						className={isActive(path)}
						data-testid={`NavbarItem_${keyIndex}`}
					>
						{/*  */}
						<Link to={path}>{title}</Link>
					</li>
				))}
			</ul>
		</nav>
	);
};

export default Navbar;
