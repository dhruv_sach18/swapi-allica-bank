const links = [
	{ path: '/', title: 'Home', keyIndex: 'h0' },
	{ path: '/characters', title: 'Characters', keyIndex: 'c1' },
	{ path: '/favourites', title: 'Favourites', keyIndex: 'f2' }
];
export default links;
