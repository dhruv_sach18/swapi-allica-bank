import React from 'react';
import './style.css';
interface PaginationProps {
	total_pages: any;
	setPage: Function;
}
const Pagination: React.FunctionComponent<PaginationProps> = ({
	total_pages,
	setPage
}) => {
	const [currentPage, setCurrentPage] = React.useState(1);
	const onChange = (e: any, page: any) => {
		e.preventDefault();
		setCurrentPage(page);
		setPage(page);

		setStartEnd();
	};
	const setStartEnd = (start?: any, end?: any) => {
		if (total_pages === 0) {
			return [];
		}
		const padding = 2;
		start = currentPage - padding;
		end = currentPage + padding;
		if (start < 1) {
			end = start === 0 ? end + 1 : end + padding;
			start = 1;
		}
		if (end > total_pages) {
			start =
				end - 1 === total_pages
					? start - 1
					: start - padding < 1
					? 1
					: start - padding;
			end = total_pages;
			start = start < 1 ? 1 : start;
		}

		let pageNumberLinks = [];
		for (let i = start; i <= end; i++) {
			pageNumberLinks.push(i);
		}
		return pageNumberLinks;
	};
	return (
		<ul className='pagination'>
			<li className={` ${currentPage === 1 && 'disabled'}`}>
				<button
					disabled={currentPage === 1}
					onClick={e => onChange(e, currentPage - 1)}
					className='page-link'
				>
					Previous
				</button>
			</li>
			{setStartEnd().map((item, index) => (
				<li key={index} className={` ${currentPage === item ? 'active' : ''}`}>
					<button onClick={e => onChange(e, item)} className='page-link'>
						{item}
					</button>
				</li>
			))}

			<li className={` ${currentPage === total_pages && 'disabled'}`}>
				<button
					disabled={currentPage === total_pages}
					onClick={e => onChange(e, currentPage + 1)}
					className='page-link'
				>
					Next
				</button>
			</li>
		</ul>
	);
};

export default Pagination;
