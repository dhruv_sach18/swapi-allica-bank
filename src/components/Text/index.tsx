import React from 'react';
interface TextProps
	extends React.DetailedHTMLProps<
		React.HTMLAttributes<HTMLSpanElement>,
		HTMLSpanElement
	> {
	color?: string;
	weight?: number;
	size?: number;
	block?: boolean;
}
export default function Text({
	color,
	weight,
	size,
	block = true,
	children
}: TextProps) {
	return (
		<span
			data-testid='Text'
			style={{
				color,
				fontWeight: weight,
				fontSize: size,
				display: block ? 'block' : ''
			}}
		>
			{children}
		</span>
	);
}
