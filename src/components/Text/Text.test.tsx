import React from 'react';
import { render, screen } from '@testing-library/react';
import Text from '.';

test('Renders Text component', () => {
	const { getByTestId } = render(<Text />);
	expect(getByTestId('Text')).toMatchSnapshot();
});

test('Renders Children', () => {
	const { getByText } = render(<Text>Test Children</Text>);
	expect(getByText('Test Children')).toMatchSnapshot();
});

test('Renders Style Props', () => {
	const { getByTestId } = render(
		<Text color={'blue'} weight={200} size={12} />
	);
	expect(getByTestId('Text')).toHaveStyle(
		'color:blue;font-weight:200;font-size:12px'
	);
});
