// Added a constant color file for react components
// We can also do it with classes and scss var's but due to less time using this approach
const colors = {
	text: { yellow: '#ebd800', white: '#fff' }
};
export default colors;
