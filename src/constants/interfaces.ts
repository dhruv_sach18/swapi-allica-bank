// All common Interface are added here
export interface Character {
	name: string;
	gender: string;
	homeworld?: string;
	url: string;
}
